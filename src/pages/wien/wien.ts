import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { FormPage} from "../form/form";


@Component({
  selector: 'page-wien',
  templateUrl: 'wien.html',
})
export class WienPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goForm(){

    this.navCtrl.push(FormPage);
  }

}
