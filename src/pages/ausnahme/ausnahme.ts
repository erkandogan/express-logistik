import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormAusnahmePage} from "../form-ausnahme/form-ausnahme";


@Component({
  selector: 'page-ausnahme',
  templateUrl: 'ausnahme.html',
})
export class AusnahmePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goForm(){

    this.navCtrl.push(FormAusnahmePage);
  }

}
