import { Component } from '@angular/core';
import { ViewController, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private view:ViewController) {
  }

  closeTS()
  {this.view.dismiss()};

}
