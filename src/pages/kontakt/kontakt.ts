import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Toast} from "@ionic-native/toast";
import {EmailComposer} from "@ionic-native/email-composer";

@Component({
  selector: 'page-kontakt',
  templateUrl: 'kontakt.html',
})
export class KontaktPage {

  sendMail = "office@express-logistik.at";
  vorname:any;
  nachname:any;
  phone:any;
  message:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private emailComposer: EmailComposer) {
  }

post(){
  this.emailComposer.isAvailable().then((available: boolean) =>{
    if(available) {
    }
  });

  let email = {
    to: this.sendMail,
    attachments: [],
    subject: 'Kontakt'+':  '+this.vorname+'  '+this.nachname,
    body: 'Message: '+this.message+ 'TelefonNummer: '+this.phone,
    isHtml: true
  };

  this.emailComposer.open(email);

}


}
